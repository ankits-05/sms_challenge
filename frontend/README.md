# frontend
 
This creates a frontend server which when started opens a app in the browser. All the columns of the table are sortable in ascending as well as descending order. The application has two date pickers which when selected and submitted the table will show the data for which the start date and end date match to the selected dates.


## Installation

cd into the frontend folder.

### Install the node modules

```bash
npm install
```

### Start the frontend server

```bash
yarn start
```