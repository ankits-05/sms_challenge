import React from 'react';
import axios from 'axios';
import moment from 'moment';

class AppContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [{
                id: '',
                city: '',
                start_date: '',
                end_date: '',
                price: '',
                status: '',
                color: ''
            }],
            sortAsc: false,
            startDate: '',
            endDate: ''
        };

        this.onSort = this.onSort.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
    }

    componentDidMount() {
        axios.get('http://localhost:4000/api/v1/data')
        .then(res => this.setState({ 'data': res.data.data }))
      }

      onSort(event, sortCriteria) {
          const data = this.state.data;

          if (sortCriteria !== 'id') {
            if (this.state.sortAsc) {
                data.sort((a,b) => b[sortCriteria].localeCompare(a[sortCriteria]))
                this.setState({data, sortAsc: false})
              } else {
                data.sort((a,b) => a[sortCriteria].localeCompare(b[sortCriteria]))
                this.setState({data, sortAsc: true})
              }
          }
    }

    renderTableHeader() {
        let header = Object.keys(this.state.data[0])
        return header.map((key, index) => {
           return <th key={index} onClick={e => this.onSort(e, key)}>{key.toUpperCase()}</th>
           
        })
     }

     renderTableData() {
        return this.state.data.map((dataRecord, index) => {
           const { id, city, start_date, end_date, price, status, color } = dataRecord //destructuring
           return (
              <tr key={id}>
                  <td>{id}</td>
                 <td>{city}</td>
                 <td>{start_date}</td>
                 <td>{end_date}</td>
                 <td>{price}</td>
                 <td>{status}</td>
                 <td>{color}</td>
              </tr>
           )
        })
     }

     applyFilter() {
        const data = this.state.data;
        const filteredData = data.filter((date) => {
            return moment(date.start_date).format('MM/DD/YYYY') === moment(this.state.startDate).format('MM/DD/YYYY') && moment(date.end_date).format('MM/DD/YYYY') === moment(this.state.endDate).format('MM/DD/YYYY');
        });
        if (filteredData.length > 0) {
            this.setState({data: filteredData});
        }
     }
  
     render() {
        return (
           <div>
              <h1 id='title'>SMS Challenge</h1>
              <p> Start Date:
                  <p>
                     <input type="date" id="start_date" name="start_date" onChange={(event) => this.setState({startDate: event.target.value})}></input>
                  </p>
              </p>
                  <p> End Date:
                     <p>
                        <input type="date" id="end_date" name="end_date" onChange={(event) => this.setState({endDate: event.target.value})}></input>
                     </p>
                  </p>
              <input type="submit" onClick={e => this.applyFilter()}></input>
              <table id='data'>
                 <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                 </tbody>
              </table>
           </div>
        )
     }
}

export default AppContainer;
