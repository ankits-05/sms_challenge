'use strict';

import * as helper from '../model_test/helper';
import db from '../../src/data/models';

require('../mocha.conf.js');

describe('The data model test:', () => {
  before(async () => {
    await db.data.sync();
  });

  after(async () => {
    await db.data.destroy({
      where: {},
      force: true
    });
  });

  it('should initially have no data', () => {
    return expect(db.data.findAll()).to.eventually.have.length(0);
  });

  it('should insert data successfully', () => {
    const newData = helper.genData();
    return expect(newData.save()).to.be.fulfilled;
  });

  it('should fail when city is not entered', () => {
    const newData = helper.genData();
    newData.city = null;
    return expect(newData.save()).to.be.rejected;
  });

  it('should fail when start_date is not entered', () => {
    const newData = helper.genData();
    newData.start_date = null;
    return expect(newData.save()).to.be.rejected;
  });

  it('should fail when end_date is not present', () => {
    const newData = helper.genData();
    newData.end_date = null;
    return expect(newData.save()).to.be.rejected;
  });

  it('should fail when price is not present', () => {
    const newData = helper.genData();
    newData.price = null;
    return expect(newData.save()).to.be.rejected;
  });

  it('should fail when status is not present', () => {
    const newData = helper.genData();
    newData.status = null;
    return expect(newData.save()).to.be.rejected;
  });

  it('should fail when color is not present for updated_by', () => {
    const newData = helper.genData();
    newData.color = null;
    return expect(newData.save()).to.be.rejected;
  });
  
  it('should fail when color is already present', () => {
    const newData = helper.genData();
    newData.color = helper.newData;
    return expect(newData.save()).to.be.rejected;
  });
});