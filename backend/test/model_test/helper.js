'use strict';

import db from '../../src/data/models';

export const newData = {
  city: 'testcity',
  start_date: '02/02/2016',
  end_date: '02/02/2019',
  price: '19.02',
  status: 'Once',
  color: '#000000'
};

export const genData = () => {
  return db.data.build(newData);
};