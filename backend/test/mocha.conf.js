'use strict';

// Register the Babel require hook
require('babel-core/register')({
});

const chai = require('chai');

// Load Chai assertions
global.expect = chai.expect;
global.assert = chai.assert;
global.faker = require('faker');
chai.should();

chai.use(require('chai-as-promised'));
chai.use(require('chai-things'));
