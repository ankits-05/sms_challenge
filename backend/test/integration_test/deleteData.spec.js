'use strict';

import * as CONSTANTS from '../constants_test.js';
import { application } from '../../src/index';
import db from '../../src/data/models';
import request from 'supertest';
import { expect } from 'chai';

let appReq;

application.then((data) => {
  appReq = data;
});

describe('Data DELETE API:/api/v1/data', function () {
  let dataRecord;
  const newData = {
    id: 1,
    city: 'test2',
    start_date: '8/15/2014',
    end_date: '2/16/2013',
    price: '12.03',
    status: 'Daily',
    color: '#0f4cdf'
  };

  /* Before hook for setting up the database and inserting basic details required
   for the test case
  */
  before(async () => {
    await db.data.sync();

    dataRecord = db.data.build(newData);
    await dataRecord.save();
  });

  // After hook for deleting all the records from the database
  after(async () => {
    await db.data.destroy({
      where: {},
      force: true
    });
  });

  it('should respond with success message for delete data',
    (done) => {
      try {
        request(appReq)
          .delete(`${CONSTANTS.URLS_FOR_TEST.DATA_URL}/1`)
          .end(function (err, res) {
            expect(res.body.message).to.equal(CONSTANTS.SUCCESS_MESSAGES.RECORD_DELETION_SUCCESSFUL);
            done();
          });
      } catch (e) {
        done(e);
      }
    });

    it('should respond with error message for delete data if id not exist',
    (done) => {
      try {
        request(appReq)
          .delete(`${CONSTANTS.URLS_FOR_TEST.DATA_URL}/2`)
          .end(function (err, res) {
            expect(res.body.message).to.equal(CONSTANTS.ERROR_MESSAGES.NO_DATA_EXIST);
            done();
          });
      } catch (e) {
        done(e);
      }
    });
});