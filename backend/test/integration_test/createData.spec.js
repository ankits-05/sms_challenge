'use strict';

import * as CONSTANTS from '../constants_test.js';
import { application } from '../../src/index';
import db from '../../src/data/models';
import request from 'supertest';
import { expect } from 'chai';

let appReq;

application.then((data) => {
  appReq = data;
});

describe('Data POST API:/api/v1/data', function () {
  const newDataRecord = {
    city: 'test1',
    start_date: '8/15/2014',
    end_date: '2/16/2013',
    price: '12.03',
    status: 'Daily',
    color: '#0f4cdd'
  };

  /* Before hook for setting up the database and inserting basic details required
   for the test case
  */
  before(async () => {
    await db.data.sync();
  });

  // After hook for deleting all the records from the database
  after(async () => {
    await db.data.destroy({
      where: {},
      force: true
    });
  });

  it('should respond with success message for creating data',
    (done) => {
      try {
        request(appReq)
          .post(`${CONSTANTS.URLS_FOR_TEST.DATA_URL}`)
          .send(newDataRecord)
          .end(function (err, res) {
            expect(res.body).to.be.an('object');
            expect(res.body.message).to.equal(CONSTANTS.SUCCESS_MESSAGES.DATA_CREATION_SUCCESSFUL);
            expect(res.body).to.have.key('id', 'city', 'start_date', 'end_date', 'price', 'status', 'color', 'message')
            done();
          });
      } catch (e) {
        done(e);
      }
    });

    it('should respond with error message for creating data if color already exists',
    (done) => {
      try {
        request(appReq)
          .post(`${CONSTANTS.URLS_FOR_TEST.DATA_URL}`)
          .send(newDataRecord)
          .end(function (err, res) {
            expect(res.body).to.be.an('object');
            expect(res.body.message).to.equal(CONSTANTS.ERROR_MESSAGES.COLOR_ALREADY_EXISTS);
            done();
          });
      } catch (e) {
        done(e);
      }
    });
});