'use strict';

import * as CONSTANTS from '../constants_test.js';
import { application } from '../../src/index';
import db from '../../src/data/models';
import request from 'supertest';
import { expect } from 'chai';

let appReq;

application.then((data) => {
  appReq = data;
});

describe('Data PUT API:/api/v1/data', function () {
  let dataRecord;
  const dataToUpdate = {
    id: 1,
    city: 'test1',
    start_date: '8/15/2014',
    end_date: '2/16/2013',
    price: '12.03',
    status: 'Daily',
    color: '#0f4cde'
  };

  const newData = {
    id: 1,
    city: 'test2',
    start_date: '8/15/2014',
    end_date: '2/16/2013',
    price: '12.03',
    status: 'Daily',
    color: '#0f4cdf'
  };

  /* Before hook for setting up the database and inserting basic details required
   for the test case
  */
  before(async () => {
    await db.data.sync();

    dataRecord = db.data.build(dataToUpdate);
    await dataRecord.save();
  });

  // After hook for deleting all the records from the database
  after(async () => {
    await db.data.destroy({
      where: {},
      force: true
    });
  });

  it('should respond with success message for updating data',
    (done) => {
      try {
        request(appReq)
          .put(`${CONSTANTS.URLS_FOR_TEST.DATA_URL}`)
          .send(newData)
          .end(function (err, res) {
            expect(res.body).to.be.an('object');
            expect(res.body.message).to.equal(CONSTANTS.SUCCESS_MESSAGES.DATA_UPDATION_SUCCESSFUL);
            expect(res.body).to.have.key('id', 'city', 'start_date', 'end_date', 'price', 'status', 'color', 'message');
            done();
          });
      } catch (e) {
        done(e);
      }
    });

  it('should respond with error message for creating data if color already exists',
    (done) => {
      newData.id = 2;
      try {
        request(appReq)
          .put(`${CONSTANTS.URLS_FOR_TEST.DATA_URL}`)
          .send(newData)
          .end(function (err, res) {
            expect(res.body).to.be.an('object');
            expect(res.body.message).to.equal(CONSTANTS.ERROR_MESSAGES.NO_DATA_EXIST);
            done();
          });
      } catch (e) {
        done(e);
      }
    });
});