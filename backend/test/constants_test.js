'use strict';

export const URLS_FOR_TEST = {
  DATA_URL: '/api/v1/data'
};

export const SUCCESS_MESSAGES = {
  DATA_CREATION_SUCCESSFUL: 'Data created successfully',
  DATA_UPDATION_SUCCESSFUL: 'Data updated successfully',
  RECORD_DELETION_SUCCESSFUL: 'Record deleted successfully'
};

export const ERROR_MESSAGES = {
  COLOR_ALREADY_EXISTS: 'Color already exists',
  NO_DATA_EXIST: 'Data with this id does not exist'
};