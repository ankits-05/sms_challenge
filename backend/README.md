# backend
 
This creates a backend server which exposes CRUD APIs.

## Installation

cd into the backend folder.

### Install the node modules

```bash
npm install
```

### Create the database

```bash
npm run setup_db
```

### Start the backend server to create the database tables

```bash
npm start
```

Kill the node server.

### Seed the database

```bash
npm run setup
```

### Start the backend server

```bash
run npm start
```

### To run the test cases

```bash
npm run test
```