'use strict';

export const SUCCESS_MESSAGES = {
  DATA_CREATION_SUCCESSFUL: 'Data created successfully',
  DATA_UPDATION_SUCCESSFUL: 'Data updated successfully',
  RECORD_DELETION_SUCCESSFUL: 'Record deleted successfully'
};

export const ERROR_MESSAGES = {
  GENERIC_MESSAGE: 'Something went wrong.',
  DATA_CREATION_FAILED: 'Data creation failed',
  COLOR_ALREADY_EXISTS: 'Color already exists',
  NO_DATA_EXIST: 'Data with this id does not exist',
  DATA_UPDATION_FAILED: 'Data updation failed'
};