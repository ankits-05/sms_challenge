'use strict';

export const DEFINITION_TITLE = 'API Documentation';
export const VERSION = '1.0.0';
export const CONTACT_EMAIL = 'ankit.singh600@gmail.com';
export const DESCRIPTION = 'Node APIs';