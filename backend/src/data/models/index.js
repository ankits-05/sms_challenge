/**
 * Created by ankit on 03/03/20.
 * Database connection file for sequelize
 */

'use strict';

import * as CONSTANTS from 'src/common/constants/sequelize';
import fs from 'fs';
import path from 'path';
import { Sequelize } from 'sequelize';
import middlewareEnvVarLoader from 'src/service/middleware/middleware-env-var-loader';
middlewareEnvVarLoader();

const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('config/sequelize/sequelize')[env];
const db = {};

let sequelize;
if (dbConfig.use_env_variable) {
  sequelize = new Sequelize(process.env[dbConfig.use_env_variable]);
} else {
  sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, dbConfig);
}

fs.readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function (file) {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log(CONSTANTS.DB_CONNECTION_SUCCESS);
  })
  .catch(err => {
    console.log(err);
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
