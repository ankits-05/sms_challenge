'use strict';

const fs = require('fs');
const path = require('path');
const seedData = fs.readFileSync(path.join(__dirname, '../json_files/data.json')).toString();

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('data', JSON.parse(seedData), {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};