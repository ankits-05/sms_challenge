'use strict';

import db from '../../../data/models';

export const checkIfCityExist = async (city) => {
  const cityDetails = await db.data.findOne({
    where: {
      city: city
    }
  });

  return cityDetails;
};

export const checkIfColorExist = async (color) => {
  const colorDetails = await db.data.findOne({
    where: {
      color: color
    }
  });
  
  return colorDetails;
};

export const createData = async (dataToCreate, t) => {
  const createdDetails = await db.data.create(dataToCreate, {
    transaction: t
  });
    
  return createdDetails;
};

export const checkIfDataExist = async (dataId) => {
  const dataRecord = await db.data.findOne({
    where: {
      id: dataId
    }
  });
    
  return dataRecord;
};

export const updateData = async (existingData, dataToUpdate, t) => {
  const updatedRecord = await existingData.update(dataToUpdate, {
    transaction: t
  });

  return updatedRecord;
};

export const fetchAllData = async () => {
  const allData = await db.data.findAll({});

  return allData;
};

export const deleteRecord = async (dataId, t) => {
  const deletedRecord = await db.data.destroy({
    where: {
      id: dataId
    }
  }, {
    transaction: t
  });

  return deletedRecord;
};