'use strict';

import swaggerSpec from 'config/docs/swagger';

export default (app) => {
  app.get('/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
};