'use strict';

import express from 'express';
import middlewareCors from 'src/service/middleware/middleware-cors';
import middlewareEnvVarLoader from 'src/service/middleware/middleware-env-var-loader';
import middlewareRequestParser from 'src/service/middleware/middleware-request-parser';
import middlewareRoutes from 'src/service/middleware/middleware-routes';
import middlewareSwagger from 'src/service/middleware/middleware-swagger';

export default async function () {
  const app = express();
  middlewareEnvVarLoader();  
  middlewareRequestParser(app);
  middlewareCors(app);
  middlewareSwagger(app, express);
  middlewareRoutes(app);
  return app;
}
