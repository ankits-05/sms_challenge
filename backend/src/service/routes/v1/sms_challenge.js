'use strict';

import * as smsChallengeController from 'src/service/controllers/v1/sms_challenge.controller';
import { Router } from 'express';

const router = Router();

// sms challenge routes
router.post('/', smsChallengeController.createData);
router.put('/', smsChallengeController.updateData);
router.get('/:id', smsChallengeController.getData);
router.get('/', smsChallengeController.getAllData);
router.delete('/:id', smsChallengeController.deleteData);

export default router;