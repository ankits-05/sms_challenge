import cors from 'cors';

export default function middlewareCors (app) {
  app.options('*', cors());  
  app.use(cors());
}