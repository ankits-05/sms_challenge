import { json, urlencoded } from 'body-parser';

export default (app) => {
  app.use(json({
    limit: '50mb'
  }));
  app.use(urlencoded({
    limit: '50mb',
    extended: true
  }));
};
