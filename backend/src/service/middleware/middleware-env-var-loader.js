import dotenv from 'dotenv';

export default () => {
  const envfile = process.env.NODE_ENV === 'production'
    ? '.env' // production
    : '.dev.env'; // development

  // load the contents of the env file into
  // the `process.env` object.
  dotenv.config({
    silent: true,
    path: `${process.env.INIT_CWD}/${envfile}`
  });
};