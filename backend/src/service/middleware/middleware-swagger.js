'use strict';

import path from 'path';

export default function middlewareSwagger (app, express) {
  app.use(express.static(path.join(process.cwd(), '/public')));
  app.set('views', path.join(process.cwd(), '/public/'));
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
}