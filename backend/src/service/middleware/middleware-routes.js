'use strict';

import routes from 'src/routes';

export default function middlewareRoutes (app) {
  app.use('/api', routes);
}