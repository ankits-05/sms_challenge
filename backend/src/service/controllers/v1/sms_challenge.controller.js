'use strict';

import * as DATACONSTANTS from 'src/common/constants/data';
import * as dataHelper from 'src/utils/helpers/data/data';
import assert from 'assert';
import db from 'src/data/models';
import { pick } from 'underscore';

/**
 * @swagger
 * /api/v1/data:
 *   post:
 *     tags:
 *       - data
 *     description: Creates data
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Data for new record creation
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/create_data_req'
 *     responses:
 *       200:
 *         description: success
 *         schema:
 *           $ref: '#/definitions/create_data_success'
 *       400:
 *         description: error
 *         schema:
 *           $ref: '#/definitions/create_data_error'
 */

/**
 * @swagger
 * definition:
 *   create_data_req:
 *     properties:
 *       city:
 *         type: string
 *         required: true
 *       start_date:
 *         type: string
 *         required: true
 *       end_date:
 *         type: string
 *         required: true
 *       price:
 *         type: string
 *         required: true
 *       status:
 *         type: string
 *         required: true
 *       color:
 *         type: string
 *         required: true
 *
 *   create_data_success:
 *     properties:
 *       message:
 *         type: string
 *       id:
 *         type: integer
 *       city:
 *         type: string
 *         required: true
 *       start_date:
 *         type: string
 *         required: true
 *       end_date:
 *         type: string
 *         required: true
 *       price:
 *         type: string
 *         required: true
 *       status:
 *         type: string
 *         required: true
 *       color:
 *         type: string
 *         required: true
 * 
 *   create_data_error:
 *     properties:
 *       message:
 *         type: string
 */

export const createData = (req, res) => {
  const requestData = pick(req.body, 'city', 'start_date', 'end_date', 'price',
    'status', 'color');
  return db.sequelize.transaction(async (t) => {
    const isColorExist = await dataHelper.checkIfColorExist(requestData.color);
    assert(!isColorExist, DATACONSTANTS.ERROR_MESSAGES.COLOR_ALREADY_EXISTS);

    const createdData = await dataHelper.createData(requestData, t);
    assert(createdData, DATACONSTANTS.ERROR_MESSAGES.DATA_CREATION_FAILED);

    return ({
      id: createdData.id,
      city: createdData.city,
      start_date: createdData.start_date,
      end_date: createdData.end_date,
      price: createdData.price,
      status: createdData.status,
      color: createdData.color
    });
  }).then((response) => {
    response.message = DATACONSTANTS.SUCCESS_MESSAGES.DATA_CREATION_SUCCESSFUL;
    res.status(200).json(response);
  }).catch(onError);

  function onError (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      res.status(409).json({
        message: err.errors[0].message
      });
    } else if (err instanceof ReferenceError || err instanceof SyntaxError || 
          err instanceof TypeError || err instanceof RangeError) {
      res.status(500).json({
        message: DATACONSTANTS.ERROR_MESSAGES.GENERIC_MESSAGE
      });
    } else {
      res.status(400).json({
        message: err.message
      });
    }
  }
};

/**
 * @swagger
 * /api/v1/data:
 *   put:
 *     tags:
 *       - data
 *     description: Updates data
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: Data for updating existing record
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/update_data_req'
 *     responses:
 *       200:
 *         description: success
 *         schema:
 *           $ref: '#/definitions/update_data_success'
 *       400:
 *         description: error
 *         schema:
 *           $ref: '#/definitions/update_data_error'
 */

/**
 * @swagger
 * definition:
 *   update_data_req:
 *     properties:
 *       id:
 *         type: integer
 *       city:
 *         type: string
 *       start_date:
 *         type: string
 *       end_date:
 *         type: string
 *       price:
 *         type: string
 *       status:
 *         type: string
 *       color:
 *         type: string
 *
 *   update_data_success:
 *     properties:
 *       message:
 *         type: string
 *       id:
 *         type: integer
 *       city:
 *         type: string
 *       start_date:
 *         type: string
 *       end_date:
 *         type: string
 *       price:
 *         type: string
 *       status:
 *         type: string
 *       color:
 *         type: string
 * 
 *   update_data_error:
 *     properties:
 *       message:
 *         type: string
 */

export const updateData = (req, res) => {
  const requestData = pick(req.body, 'id', 'city', 'start_date', 'end_date', 'price',
    'status', 'color');
  return db.sequelize.transaction(async (t) => {
    const existingData = await dataHelper.checkIfDataExist(requestData.id);
    assert(existingData, DATACONSTANTS.ERROR_MESSAGES.NO_DATA_EXIST);

    const updatedData = await dataHelper.updateData(existingData, requestData, t);
    assert(updatedData, DATACONSTANTS.ERROR_MESSAGES.DATA_UPDATION_FAILED);

    return ({
      id: updatedData.id,
      city: updatedData.city,
      start_date: updatedData.start_date,
      end_date: updatedData.end_date,
      price: updatedData.price,
      status: updatedData.status,
      color: updatedData.color
    });
  }).then((response) => {
    response.message = DATACONSTANTS.SUCCESS_MESSAGES.DATA_UPDATION_SUCCESSFUL;
    res.status(200).json(response);
  }).catch(onError);

  function onError (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      res.status(409).json({
        message: err.errors[0].message
      });
    } else if (err instanceof ReferenceError || err instanceof SyntaxError || 
          err instanceof TypeError || err instanceof RangeError) {
      res.status(500).json({
        message: DATACONSTANTS.ERROR_MESSAGES.GENERIC_MESSAGE
      });
    } else {
      res.status(400).json({
        message: err.message
      });
    }
  }
};

/**
 * @swagger
 * /api/v1/data/{id}:
 *   get:
 *     tags:
 *       - data
 *     description: get data for a particular id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id for which record is to be fetched
 *         in: params
 *         required: true
 *     responses:
 *       200:
 *         description: success
 *         schema:
 *           $ref: '#/definitions/get_data_success'
 *       404:
 *         description: error
 *         schema:
 *           $ref: '#/definitions/get_data_error'
 */

/**
 * @swagger
 * definition:
 *   get_data_success:
 *     properties:
 *       data:
 *         type: object
 *         properties:
 *           id:
 *             type: integer
 *           city:
 *             type: string
 *           start_date:
 *             type: string
 *           end_date:
 *             type: string
 *           price:
 *             type: string
 *           status:
 *             type: string
 *           color:
 *             type: string
 * 
 *   get_data_error:
 *     properties:
 *       message:
 *         type: string
 */

export const getData = (req, res) => {
  return db.sequelize.transaction(async (t) => {
    const existingData = await dataHelper.checkIfDataExist(req.params.id);

    return ({
      data: existingData || {}
    });
  }).then((response) => {
    res.status(200).json(response);
  }).catch(onError);

  function onError (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      res.status(409).json({
        message: err.errors[0].message
      });
    } else if (err instanceof ReferenceError || err instanceof SyntaxError || 
          err instanceof TypeError || err instanceof RangeError) {
      res.status(500).json({
        message: DATACONSTANTS.ERROR_MESSAGES.GENERIC_MESSAGE
      });
    } else {
      res.status(404).json({
        message: err.message
      });
    }
  }
};

/**
 * @swagger
 * /api/v1/data:
 *   get:
 *     tags:
 *       - data
 *     description: get all data
 *     produces:
 *       - application/json
 *     parameters:
 *
 *     responses:
 *       200:
 *         description: success
 *         schema:
 *           $ref: '#/definitions/get_all_data_success'
 *       404:
 *         description: error
 *         schema:
 *           $ref: '#/definitions/get_all_data_error'
 */

/**
 * @swagger
 * definition:
 *   get_all_data_success:
 *     properties:
 *       data:
 *         type: object
 *         properties:
 *           id:
 *             type: integer
 *           city:
 *             type: string
 *           start_date:
 *             type: string
 *           end_date:
 *             type: string
 *           price:
 *             type: string
 *           status:
 *             type: string
 *           color:
 *             type: string
 * 
 *   get_all_data_error:
 *     properties:
 *       message:
 *         type: string
 */

export const getAllData = (req, res) => {
  return db.sequelize.transaction(async (t) => {
    const existingData = await dataHelper.fetchAllData();

    return ({
      data: existingData || []
    });
  }).then((response) => {
    res.status(200).json(response);
  }).catch(onError);

  function onError (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      res.status(409).json({
        message: err.errors[0].message
      });
    } else if (err instanceof ReferenceError || err instanceof SyntaxError || 
          err instanceof TypeError || err instanceof RangeError) {
      res.status(500).json({
        message: DATACONSTANTS.ERROR_MESSAGES.GENERIC_MESSAGE
      });
    } else {
      res.status(404).json({
        message: err.message
      });
    }
  }
};

/**
 * @swagger
 * /api/v1/data/{id}:
 *   delete:
 *     tags:
 *       - data
 *     description: deletes a record
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: record id which is to be deleted
 *         in: params
 *         required: true
 *     responses:
 *       200:
 *         description: success
 *         schema:
 *           $ref: '#/definitions/delete_data_success'
 *       404:
 *         description: error
 *         schema:
 *           $ref: '#/definitions/delete_data_error'
 */

/**
 * @swagger
 * definition:
 *   delete_data_success:
 *     properties:
 *       message:
 *         type: string
 * 
 *   delete_data_error:
 *     properties:
 *       message:
 *         type: string
 */

export const deleteData = (req, res) => {
  return db.sequelize.transaction(async (t) => {
    const existingData = await dataHelper.checkIfDataExist(req.params.id);
    assert(existingData, DATACONSTANTS.ERROR_MESSAGES.NO_DATA_EXIST);

    const deletedRecord = await dataHelper.deleteRecord(req.params.id, t);
    assert(deletedRecord, DATACONSTANTS.ERROR_MESSAGES.RECORD_DELETION_FAILED);

    return ({
      message: DATACONSTANTS.SUCCESS_MESSAGES.RECORD_DELETION_SUCCESSFUL
    });
  }).then((successMessage) => {
    res.status(200).json(successMessage);
  }).catch(onError);

  function onError (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      res.status(409).json({
        message: err.errors[0].message
      });
    } else if (err instanceof ReferenceError || err instanceof SyntaxError || 
          err instanceof TypeError || err instanceof RangeError) {
      res.status(500).json({
        message: DATACONSTANTS.ERROR_MESSAGES.GENERIC_MESSAGE
      });
    } else {
      res.status(404).json({
        message: err.message
      });
    }
  }
};