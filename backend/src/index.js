'use strict';

import appLication from 'src/service/app';
import db from 'src/data/models';
import http from 'http';
import swagger from 'src/utils/swagger';

async function startServer () {
  const app = await appLication();
  const PORT = process.env.PORT || 5000;
  
  const server = http.createServer(app);
  server.timeout = 2500000; 

  const startServer = () => {
    server.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`);
    });
  };

  swagger(app);

  db.sequelize.sync().then(startServer);

  return app; 
}

export const application = startServer();