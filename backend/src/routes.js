'use strict';

import { Router } from 'express';
import smsChallengeRoutes from 'src/service/routes/v1/sms_challenge';

const router = Router();

router.use('/v1/data', smsChallengeRoutes);

export default router;