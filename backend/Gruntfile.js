'use strict';

module.exports = (grunt) => {
  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    express: 'grunt-express-server'
  });

  require('dotenv').config({ path: './.dev.env' });

  /* // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt); */
  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    pkg: grunt.file.readJSON('package.json'),
       
    mochaTest: {
      options: {
        reporter: 'spec',
        require: './test/mocha.conf.js',
        timeout: 25000 // set default mocha spec timeout
      },
      model: {
        src: ['./test/model_test/*.model.spec.js']
      },
      integration: {
        src: ['./test/integration_test/*.spec.js']
      }
    }
  });
  grunt.event.on('coverage', (lcov, done) => {
    require('coveralls').handleInput(lcov, (err) => {
      if (err) {
        return done(err);
      }
      done();
    });
  });
 
  grunt.registerTask('test', (target, option) => {
    if (target === 'server') {
      return grunt.task.run([
        // 'mochaTest:model',
        'mochaTest:integration'
      ]);
    } else {
      grunt.task.run([
        'test:server'
      ]); 
    }
  });

  grunt.registerTask('default', [
    'newer:jshint',
    'test'
  ]);
};
