'use strict';

module.exports = {
  development: {
    username: process.env.DEV_USERNAME,
    password: process.env.DEV_PASSWORD,
    database: process.env.DEV_DATABASE,
    host: process.env.DEV_HOST,
    port: process.env.DEV_PORT,
    dialect: process.env.DEV_DIALECT,
    pool: {
      max: 100,
      min: 0,
      idle: 30000,
      acquire: 80000,
      evict: 1000
    }
  },
  test: {
    username: process.env.TEST_USERNAME,
    password: process.env.TEST_PASSWORD,
    database: process.env.TEST_DATABASE,
    host: process.env.TEST_HOST,
    port: process.env.TEST_PORT,
    dialect: process.env.TEST_DIALECT,
    pool: {
      max: 100,
      min: 0,
      idle: 30000,
      acquire: 80000,
      evict: 1000
    }
  }
};