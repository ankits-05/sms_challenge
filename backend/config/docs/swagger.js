'use strict';

import * as CONSTANTS from 'src/common/constants/swagger';
import swaggerJSDoc from 'swagger-jsdoc';

const swaggerDefinition = {
  info: {
    title: CONSTANTS.DEFINITION_TITLE,
    version: CONSTANTS.VERSION,
    contact: {
      email: CONSTANTS.CONTACT_EMAIL
    },
    description: CONSTANTS.DESCRIPTION
  },
  basePath: '/'
};

const swaggerOptions = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['src/service/controllers/**/*.controller.js']
};

/**
 * Initialize swagger-jsdoc.
 */
const swaggerSpec = swaggerJSDoc(swaggerOptions);

export default swaggerSpec;